package com.example.coffeeshop.entity;

// moze lepiej rola
public enum Role {

    ROLE_ADMIN("Admin"), ROLE_USER("Waiter");

    private String roleDescription;

    private Role(String desc) {
        roleDescription = desc;
    }

    public String getRoleDescription() {
        return roleDescription;
    }

    public static Role fromString(String value) {
        for (Role role : Role.values()) {
            if (value.equalsIgnoreCase(role.name())) {
                return role;
            }
        }
        return null;
    }

    public static Role findRoleByDescription(String roleDesc) {

        for (Role role : Role.values()) {
            if (role.getRoleDescription().equals(roleDesc)) {
                return role;
            }
        }
        return null;
    }


}
