package com.example.coffeeshop.entity;

import com.example.coffeeshop.auxiliary.Status;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "orders")
public class Order {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "table_no")
    private Integer tableNo;

    //@ManyToOne
    // TODO: 2018-07-01 brakuje mi powiazania z encją user jak wyciagam dane zamowienia
    // automatycznie chce mieć wyciągnietego usera
    @Column(name = "user_id")
    private Long userId;

    @Column(name = "current_status")
    @Enumerated(EnumType.STRING)
    private Status currentStatus;

    private String comment;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "order", cascade = CascadeType.ALL)
    private List<OrderItem> orderItems;

    public Order() {
    }

    public Order(Long id, Integer tableNo, Long userId, Status currentStatus, String comment, List<OrderItem> list) {
        this.id = id;
        this.tableNo = tableNo;
        this.userId = userId;
        this.currentStatus = currentStatus;
        this.comment = comment;
        orderItems = list;
    }

    public Order(Integer tableNo, Long userId, Status currentStatus, String comment, List<OrderItem> list) {
        this.tableNo = tableNo;
        this.userId = userId;
        this.currentStatus = currentStatus;
        this.comment = comment;
        orderItems = list;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getTableNo() {
        return tableNo;
    }

    public void setTableNo(Integer tableNo) {
        this.tableNo = tableNo;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Status getCurrentStatus() {
        return currentStatus;
    }

    public void setCurrentStatus(Status currentStatus) {
        this.currentStatus = currentStatus;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public List<OrderItem> getOrderItems() {
        return orderItems;
    }

    public void setOrderItems(List<OrderItem> orderItems) {
        this.orderItems = orderItems;
    }
}