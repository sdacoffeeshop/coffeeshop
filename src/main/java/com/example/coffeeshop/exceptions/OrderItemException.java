package com.example.coffeeshop.exceptions;


public class OrderItemException extends Exception{
    public OrderItemException(String message) {
        super(message);
    }

    public OrderItemException(String message, Throwable cause) {
        super(message, cause);
    }
}
