package com.example.coffeeshop.converters;

import com.example.coffeeshop.auxiliary.Status;
import com.example.coffeeshop.dto.OrderDto;
import com.example.coffeeshop.dto.OrderItemDto;
import com.example.coffeeshop.dto.UserDto;
import com.example.coffeeshop.entity.Order;
import com.example.coffeeshop.entity.OrderItem;
import com.example.coffeeshop.exceptions.OrderItemException;
import com.example.coffeeshop.repository.OrderRepository;
import com.example.coffeeshop.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Component
public class OrderItemConverter {

    @Autowired
    private UserService userService;

    @Autowired
    private OrderRepository orderRepository;

    public OrderItemDto convertOrderItem(OrderItem orderItem) {

        Order order = orderItem.getOrder();
        OrderDto orderDto = null;
        if (order != null) {
            UserDto userDto = userService.findById(order.getUserId());
            List<OrderItemDto> orderItemDtoList = new ArrayList<>();
            orderDto = new OrderDto(
                    order.getId(),
                    order.getTableNo(),
                    userDto,
                    order.getCurrentStatus().getStatus(),
                    order.getComment(),
                    orderItemDtoList);

            List<OrderItem> orderItems = order.getOrderItems();
            OrderDto finalOrderDto = orderDto;
            StreamSupport
                    .stream(orderItems.spliterator(), false)
                    .forEach(o -> orderItemDtoList.add(new OrderItemDto(o.getId(), finalOrderDto, o.getProductName(), o.getProductPrice(), o.getQuantity())));

            OrderItemDto orderItemDto = new OrderItemDto(
                    orderItem.getId(),
                    orderDto,
                    orderItem.getProductName(),
                    orderItem.getProductPrice(),
                    orderItem.getQuantity()
            );

            return orderItemDto;
        }
        return null;
    }


    public OrderItem convertOrderItemDto(OrderItemDto orderItemDto) {
        OrderDto orderDto = orderItemDto.getOrderDto();
        List<OrderItem> items = new ArrayList<>();
        Order order = null;

        if (orderDto != null) {
            order = new Order(
                    orderDto.getId(),
                    orderDto.getTableNo(),
                    orderDto.getUserDto().getId(),
                    Status.valueOf(orderDto.getCurrentStatus()),
                    orderDto.getComment(),
                    items);

            Order finalOrder = order;
            StreamSupport.stream(orderDto.getOrderItemDtoList().spliterator(), false)
                    .forEach(orderItem -> items
                            .add(new OrderItem(
                                    orderItem.getId(),
                                    finalOrder,
                                    orderItem.getProductName(),
                                    orderItem.getProductPrice(),
                                    orderItem.getQuantity())));
        }

        OrderItem orderItem = new OrderItem(
                orderItemDto.getId(),
                order,
                orderItemDto.getProductName(),
                orderItemDto.getProductPrice(),
                orderItemDto.getQuantity()
        );
        return orderItem;
    }
}
