package com.example.coffeeshop.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class ErrorController {

    @GetMapping(value = "/403")
    public String accessDenied(ModelMap map) {
        return "errorPage";
    }

}
