package com.example.coffeeshop.controllers;

import com.example.coffeeshop.auxiliary.Status;
import com.example.coffeeshop.converters.OrderItemConverter;
import com.example.coffeeshop.dto.OrderItemDto;
import com.example.coffeeshop.dto.ProductDto;
import com.example.coffeeshop.dto.UserDto;
import com.example.coffeeshop.exceptions.OrderException;
import com.example.coffeeshop.dto.OrderDto;
import com.example.coffeeshop.exceptions.OrderItemException;
import com.example.coffeeshop.service.OrderItemService;
import com.example.coffeeshop.service.OrderService;
import com.example.coffeeshop.service.ProductService;
import com.example.coffeeshop.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.persistence.criteria.CriteriaBuilder;
import java.util.*;

@Controller
public class OrderController {

    @Autowired
    OrderService orderService;

    @Autowired
    UserService userService;

    @Autowired
    ProductService productService;
    @Autowired
    OrderItemConverter orderItemConverter;
    @Autowired
    OrderItemService orderItemService;

    private Logger logger = LoggerFactory.getLogger(ProductController.class);


    @RequestMapping(value = "/secured/user/order")
    public String findById(@RequestParam("orderId") Long orderId, Model model) throws OrderException {
        OrderDto orderDto = orderService.findById(orderId);
        model.addAttribute("orderModel", orderDto);

        List<Status> statuses = Arrays.asList(Status.values());
        model.addAttribute("statuses", statuses);
        return "orderView";
    }


    @RequestMapping(value = "/secured/user/order/save", method = RequestMethod.POST)
    public String saveOrder(@ModelAttribute(name = "orderModel") OrderDto orderDto, Model model) throws OrderException {
        Long[] orderItemsIds;
        Integer[] orderItemsQuantities;
        Map<Long,Integer> orderItemsIdAndQts = new HashMap<>();
        List<ProductDto> productDtos = new ArrayList<>();
        List<OrderItemDto> orderItemDtos = new ArrayList<>();


        try {
            orderItemsIds = orderDto.getOrderItemDtoId();
            orderItemsQuantities = orderDto.getOrderItemQuantities();

            for (int i=0; i<orderItemsIds.length;i++){
                orderItemsIdAndQts.put(orderItemsIds[i],orderItemsQuantities[i]);
            }

            for (Long id : orderItemsIds){
                productDtos.add(productService.findById(id));
            }

            for (ProductDto productDto: productDtos){
                orderItemDtos.add(new OrderItemDto(orderDto,productDto.getName(),productDto.getPrice(),orderItemsIdAndQts.get(productDto.getId())));
            }
            orderDto.setOrderItemDtoList(orderItemDtos);
            orderDto = orderService.saveOrUpdate(orderDto);
//            for (OrderItemDto orderItemDto:orderItemDtos){
//                orderItemDto.setOrderDto(orderDto);
//                orderItemService.saveOrUpdate(orderItemDto);
//            }

        } catch (OrderException ex) {
            // TODO: 2018-07-01 obsługa błędu co jak sie nie zapisze 
            logger.error("cannot save order" + orderDto);
        }
//        } catch (OrderItemException e) {
//            e.printStackTrace();
//        }
        model.addAttribute("orderModel", orderDto);
        return "redirect:/secured/user/order/list";
    }

    @GetMapping(value = "/secured/user/order/add")
    public String addOrder(Model model) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        OrderDto orderDto = new OrderDto();
        String userName = authentication.getName();
        UserDto userDto = null;
        if (userName != null) {
            userDto = userService.findByUsername(userName);
        }
        orderDto.setUserDto(userDto);
        // TODO: 2018-07-01 moze z enuma status by brac 
        orderDto.setCurrentStatus(Status.ACCEPTED.getStatus());
        model.addAttribute("orderModel", orderDto);
        List<Status> statuses = Arrays.asList(Status.values());
        model.addAttribute("statuses", statuses);
        Iterable<ProductDto> products= productService.findAll();
        model.addAttribute("productsModel", products);
        return "orderView";
    }

    @RequestMapping(value = "/secured/user/order/list")
    public String findAllOrders(Model model) {
        List<OrderDto> orders = orderService.findAll();
        model.addAttribute("orderList", orders);
        return "orderListView";
    }
}
