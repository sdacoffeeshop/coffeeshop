package com.example.coffeeshop.controllers;

import com.example.coffeeshop.dto.ProductDto;
import com.example.coffeeshop.entity.Product;
import com.example.coffeeshop.repository.ProductRepository;
import com.example.coffeeshop.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
public class ProductController {

    @Autowired
    private ProductService productService;

    @GetMapping(value = "/")
    public String rootSite(){
        return "redirect:/login";
    }

    @GetMapping(value = "/secured/user/products")
    public String findAllProducts(Model model) {
        List<ProductDto> productList = productService.findAll();
        model.addAttribute("productList", productList);
        return "productListView";
    }

    @GetMapping(value = "/secured/user/product")
    public String findById(@RequestParam("productId") Long productId, Model model) {
        ProductDto productDto = productService.findById(productId);
        model.addAttribute("productModel", productDto);
        return "productView";
    }

    @RequestMapping(value = "/secured/user/product/save", method = RequestMethod.POST)
    public String saveProduct(@ModelAttribute(name = "productModel") ProductDto productDto, Model model) {
        productDto = productService.save(productDto);
        return "redirect:/secured/user/products";
    }

    @GetMapping(value = "/secured/user/product/add")
    public String addProduct(Model model) {
        model.addAttribute("productModel", new ProductDto());
        return "productView";
    }
}