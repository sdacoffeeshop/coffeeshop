package com.example.coffeeshop.controllers;

import com.example.coffeeshop.dto.ProductDto;
import com.example.coffeeshop.dto.UserDto;
import com.example.coffeeshop.entity.Role;
import com.example.coffeeshop.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;
import java.util.List;

@Controller
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping(value = "/secured/admin/users")
    public String findAllUsers(Model model) {
        List<UserDto> userList = userService.findAll();
        model.addAttribute("userList", userList);
        return "userListView";
    }

    @GetMapping(value = "/secured/admin/user")
    public String findById(@RequestParam("userId") Long userId, Model model) {
        UserDto userDto= userService.findById(userId);
        model.addAttribute("userModel", userDto);
        // TODO: 2018-07-01 moze wstawianie słownika ról gdzies wyniesc zeby odciazyc kontroller 
        Map<String, String> roles = new HashMap<String, String>();
        roles.put(Role.ROLE_ADMIN.name(), Role.ROLE_ADMIN.name());
        roles.put(Role.ROLE_USER.name(), Role.ROLE_USER.name());
        model.addAttribute("roles", roles);
        return "userView";
    }

    @RequestMapping(value = "/secured/admin/userSave", method = RequestMethod.POST)
    public String saveUser(@ModelAttribute(name = "userModel") UserDto userDto, Model model) {
        userService.save(userDto);
        return "redirect:/secured/admin/users";
    }

    @GetMapping(value = "/secured/admin/userAdd")
    public String addUser(Model model) {
        model.addAttribute("userModel", new UserDto());
        // TODO: 2018-07-01 powtarzalne wstwianie ról 
        Map<String, String> roles = new HashMap<String,String>();
        roles.put(Role.ROLE_ADMIN.name(), Role.ROLE_ADMIN.name());
        roles.put(Role.ROLE_USER.name(), Role.ROLE_USER.name());
        model.addAttribute("roles", roles);
        return "userView";
    }
}