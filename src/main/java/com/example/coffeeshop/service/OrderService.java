package com.example.coffeeshop.service;

import com.example.coffeeshop.exceptions.OrderException;
import com.example.coffeeshop.dto.OrderDto;

import java.util.List;

public interface OrderService {

    public OrderDto findById(Long id) throws OrderException;

    public OrderDto saveOrUpdate(OrderDto orderDto) throws OrderException;

    public List<OrderDto> findAll();

}
