package com.example.coffeeshop.service;


import com.example.coffeeshop.dto.UserDto;
import com.example.coffeeshop.entity.Role;
import com.example.coffeeshop.entity.User;
import com.example.coffeeshop.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.apache.commons.codec.digest.DigestUtils;

import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service("userService")
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public List<UserDto> findAll() {
        Iterable<User> users = userRepository.findAll();
        return StreamSupport
                .stream(users.spliterator(), false)
                .map(new UserConverter()).collect(Collectors.toList());
    }

    @Override
    // TODO: 2018-07-01 brakuje mi troche obsługi wyjatkow, co jak nie znajdę uzytkownika
    public UserDto findById(Long id) {
        UserConverter userConverter = new UserConverter();
        Optional<User> user = userRepository.findById(id);
        UserDto userDto = null;
        if (user.isPresent()) {
            userDto = userConverter.apply(user.get());
        }
        return userDto;
    }

    @Override
    public UserDto findByUsername(String name) {
        UserConverter userConverter = new UserConverter();

        Optional<User> user = userRepository.findOneByUsername(name);
        UserDto userDto = null;
        if (user.isPresent()) {
            userDto = userConverter.apply(user.get());
        }
        return userDto;
    }

    @Override
    // TODO: 2018-07-01 brakuje mi troche obsługi wyjatkow, co ja wystąpi błąd przy zapisie
    public UserDto save(UserDto userDto) {

        UserDtoConverter userDtoConverter = new UserDtoConverter();
        User user = userDtoConverter.apply(userDto);
        if (userDto.getId() == null) {
            user = userRepository.save(user);
        } else {
            Role role = Role.fromString(userDto.getRole());
            userRepository.updateUser(
                    userDto.getId(),
                    userDto.getUsername(),
                    userDto.getName(),
                    role,
                    userDto.getEnabled());
            user = userRepository.getOne(userDto.getId());
        }
        UserConverter userConverter = new UserConverter();

        return userConverter.apply(user);
    }


    class UserConverter implements Function<User, UserDto> {

        @Override
        public UserDto apply(User user) {

            UserDto userDto = new UserDto(
                    user.getId(),
                    user.getUsername(),
                    user.getName(),
                    user.getRole().toString(),
                    user.getEnabled());
            return userDto;
        }
    }

    class UserDtoConverter implements Function<UserDto, User> {

        @Override
        public User apply(UserDto userDto) {
            User user = new User();
            Role role = Role.fromString(userDto.getRole());

            user.setId(userDto.getId());
            user.setName(userDto.getName());
            user.setUsername(userDto.getUsername());
            user.setPassword(DigestUtils.md5Hex(userDto.getPassword()));
            user.setRole(role);
            user.setEnabled(userDto.getEnabled());
            return user;
        }
    }

    @Override
    public boolean authenticate(String username, String password) {

        password = DigestUtils.md5Hex(password);//bCryptPasswordEncoder().encode(password);
        User user = userRepository.findOneByUsernameAndPasswordAndEnabled(username, password, true);
        return user != null ? true : false;
    }

}
