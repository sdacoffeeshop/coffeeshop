package com.example.coffeeshop.service;

import com.example.coffeeshop.dto.OrderItemDto;
import com.example.coffeeshop.exceptions.OrderItemException;

import java.util.List;

public interface OrderItemService {
    public OrderItemDto findById(Long id) throws OrderItemException;

    public OrderItemDto saveOrUpdate(OrderItemDto orderItemDto) throws OrderItemException;

    public List<OrderItemDto> findAll() throws OrderItemException;
}
