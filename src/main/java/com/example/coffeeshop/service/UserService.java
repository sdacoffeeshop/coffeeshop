package com.example.coffeeshop.service;

import com.example.coffeeshop.dto.ProductDto;
import com.example.coffeeshop.dto.UserDto;
import com.example.coffeeshop.entity.User;

import java.util.List;

public interface UserService {

    List<UserDto> findAll();
    UserDto findById(Long id);
    UserDto findByUsername(String name);
    UserDto save(UserDto userDto);

    boolean authenticate(String username, String password);
}
