package com.example.coffeeshop.service;


import com.example.coffeeshop.dto.ProductDto;
import com.example.coffeeshop.entity.Product;
import com.example.coffeeshop.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service("productService")
public class ProductServiceImpl implements ProductService{

    @Autowired
    private ProductRepository productRepository;

    @Override
    public List<ProductDto> findAll() {
        Iterable<Product> products = productRepository.findAll();
        return StreamSupport
                .stream(products.spliterator(),false)
                .map(new ProductConverter()).collect(Collectors.toList());
    }

    @Override
    public ProductDto findById(Long productId) {

        Optional<Product> product = productRepository.findById(productId);
        Optional<ProductDto> productDto = product.map(new ProductConverter());

        return productDto.get();

    }

    @Override
    public ProductDto save(ProductDto productDto) {
        ProductDtoConverter productDtoConverter = new ProductDtoConverter();
        Product product = productDtoConverter.apply(productDto);
        product = productRepository.save(product);
        ProductConverter productConverter = new ProductConverter();
        return productConverter.apply(product);
    }

    class ProductConverter implements Function<Product, ProductDto> {

        @Override
        public ProductDto apply(Product product) {
            ProductDto productDto = new ProductDto(
                    product.getId(),
                    product.getName(),
                    product.getDescription(),
                    product.getPrice());
            return productDto;
        }
    }
    class ProductDtoConverter implements Function<ProductDto,Product> {

        @Override
        public Product apply(ProductDto productDto) {
            Product product = new Product();
            product.setId((productDto.getId()));
            product.setName(productDto.getName());
            product.setPrice(productDto.getPrice());
            product.setDescription(productDto.getDescription());
            return product;
        }
    }
}
