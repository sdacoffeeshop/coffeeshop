package com.example.coffeeshop.service;

import com.example.coffeeshop.dto.ProductDto;
import com.example.coffeeshop.entity.Product;
import com.example.coffeeshop.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public interface ProductService {

    List<ProductDto> findAll();

    ProductDto findById(Long productId);

    ProductDto save(ProductDto productDto);

}
