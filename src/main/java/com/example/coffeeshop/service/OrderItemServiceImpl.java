package com.example.coffeeshop.service;

import com.example.coffeeshop.converters.OrderItemConverter;
import com.example.coffeeshop.dto.OrderItemDto;
import com.example.coffeeshop.entity.OrderItem;
import com.example.coffeeshop.exceptions.OrderItemException;
import com.example.coffeeshop.repository.OrderItemRepository;
import com.example.coffeeshop.repository.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class OrderItemServiceImpl implements OrderItemService {

    @Autowired
    private OrderItemRepository orderItemRepository;

    @Autowired
    private OrderItemConverter orderItemConverter;

    @Override
    public OrderItemDto findById(Long id) throws OrderItemException {
        Optional<OrderItem> orderItem = orderItemRepository.findById(id);
        OrderItemDto orderItemDto = orderItemConverter.convertOrderItem(orderItem.get());
        if (orderItemDto!= null) {
            return orderItemDto;
        } else {
            throw new OrderItemException("There is no such order in a database of id=" + id);
        }
    }

    @Override
    public OrderItemDto saveOrUpdate(OrderItemDto orderItemDto) throws OrderItemException {
        OrderItem orderItem = orderItemConverter.convertOrderItemDto(orderItemDto);
        orderItem = orderItemRepository.save(orderItem);
        return orderItemConverter.convertOrderItem(orderItem);
    }

    @Override
    public List<OrderItemDto> findAll() throws OrderItemException {
        Iterable<OrderItem> ordersItem = orderItemRepository.findAll();

        return StreamSupport
                .stream(ordersItem.spliterator(),false)
                .map(orderItem -> orderItemConverter.convertOrderItem(orderItem)).collect(Collectors.toList());
    }






}
