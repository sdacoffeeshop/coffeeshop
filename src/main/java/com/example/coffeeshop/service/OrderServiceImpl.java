package com.example.coffeeshop.service;

import com.example.coffeeshop.auxiliary.Status;
import com.example.coffeeshop.converters.OrderItemConverter;
import com.example.coffeeshop.dto.OrderItemDto;
import com.example.coffeeshop.dto.UserDto;
import com.example.coffeeshop.entity.OrderItem;
import com.example.coffeeshop.entity.Product;
import com.example.coffeeshop.exceptions.OrderException;
import com.example.coffeeshop.dto.OrderDto;
import com.example.coffeeshop.entity.Order;
import com.example.coffeeshop.repository.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class OrderServiceImpl implements OrderService {

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private UserService userService;
    @Autowired
    private ProductService productService;
    @Override
    public OrderDto findById(Long id) throws OrderException {
        Optional<Order> order = orderRepository.findById(id);
        Optional<OrderDto> orderDto = order.map(new OrderConverter());
        if (orderDto.isPresent()) {
            return orderDto.get();
        } else {
            throw new OrderException("There is no such order in a database of id=" + id);
        }
    }

    @Override
    public OrderDto saveOrUpdate(OrderDto orderDto) throws OrderException {

        OrderDtoConverter converter = new OrderDtoConverter();
        Order order = converter.apply(orderDto);
        orderDto.getOrderItemDtoList();
        order = orderRepository.save(order);
        OrderConverter orderConverter = new OrderConverter();
        return orderConverter.apply(order);

    }

    public List<OrderDto> findAll() {
        Iterable<Order> orders = orderRepository.findAll();
        return StreamSupport
                .stream(orders.spliterator(), false)
                .map(new OrderConverter()).collect(Collectors.toList());
    }




    class OrderConverter implements Function<Order, OrderDto> {

        @Override
        public OrderDto apply(Order order) {
            UserDto userDto = userService.findById(order.getUserId());
            List<OrderItemDto> orderItemDtoList = new ArrayList<>();

            OrderDto orderDto = new OrderDto(
                    order.getId(),
                    order.getTableNo(),
                    userDto,
                    order.getCurrentStatus().toString(),
                    order.getComment(),
                    orderItemDtoList);
            if (order.getOrderItems() != null && order.getOrderItems().size() > 0) {
                StreamSupport.stream(order.getOrderItems().spliterator(), false)
                        .forEach(item -> orderItemDtoList
                                .add(new OrderItemDto(item.getId(), orderDto, item.getProductName(), item.getProductPrice(), item.getQuantity())));
            }
            return orderDto;
        }

    }

    class OrderDtoConverter implements Function<OrderDto, Order> {
        @Autowired
        OrderItemService orderItemService;
        @Override
        public Order apply(OrderDto orderDto) {
            Status status = Status.valueOf(orderDto.getCurrentStatus());
            List<OrderItem> orderItems = new ArrayList<>();
            Order order = new Order(
                    orderDto.getId(),
                    orderDto.getTableNo(),
                    orderDto.getUserDto().getId(),
                    status,
                    orderDto.getComment(),
                    orderItems
            );

            if (orderDto.getOrderItemDtoList() != null && orderDto.getOrderItemDtoList().size() > 0) {
                StreamSupport
                        .stream(orderDto.getOrderItemDtoList().spliterator(), false)
                        .forEach(orderItem -> orderItems
                                .add(new OrderItem(orderItem.getId(), order, orderItem.getProductName(), orderItem.getProductPrice(), orderItem.getQuantity())));
            }


            return order;
        }
    }
}
