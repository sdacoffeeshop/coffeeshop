package com.example.coffeeshop.auxiliary;

public enum Status {
    ACCEPTED ("ACCEPTED"),
    ENTERED ("ENTERED"),
    PROCESSING ("PROCESSING"),
    COMPLETED ("COMPLETED");

    private String status;

    Status(String status) {
        this.status = status;
    }

    public String getStatus() {
        return this.status;
    }
}
