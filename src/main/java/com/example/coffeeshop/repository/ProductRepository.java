package com.example.coffeeshop.repository;

import com.example.coffeeshop.dto.ProductDto;
import com.example.coffeeshop.entity.Product;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ProductRepository extends CrudRepository<Product, Long> {
}
