package com.example.coffeeshop.repository;

import com.example.coffeeshop.entity.Order;
import com.example.coffeeshop.entity.OrderItem;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface OrderItemRepository extends CrudRepository<OrderItem, Long> {

    List<OrderItem> findAllByOrder(Order order);
}
