package com.example.coffeeshop.repository;


import com.example.coffeeshop.entity.Role;
import com.example.coffeeshop.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;


@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    User findOneByUsernameAndPasswordAndEnabled(String username,
                                       String password,
                                       Boolean enabled);

    Optional<User> findOneByUsername(String name);

    @Transactional
    @Modifying
    @Query("UPDATE User u SET u.username=:username , u.name=:name , u.role=:role , u.enabled=:enabled WHERE u.id =:userId")
    int updateUser(@Param("userId") Long id,
                    @Param("username") String username,
                    @Param("name") String name,
                    @Param("role") Role role,
                    @Param("enabled") Boolean enabled);
}
