package com.example.coffeeshop.providers;

import com.example.coffeeshop.dto.UserDto;
import com.example.coffeeshop.entity.Role;
import com.example.coffeeshop.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;

@Component
public class UserDatabaseAuthenticationProvider implements AuthenticationProvider {


    @Autowired
    UserService userService;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        String username=authentication.getName();
        String password=authentication.getCredentials().toString();


        if (userService.authenticate(username, password)) {
            UserDto userDto = userService.findByUsername(username);
            String userRole = userDto.getRole();

            return new
                    UsernamePasswordAuthenticationToken
                    (username, password, Arrays.asList(new SimpleGrantedAuthority(userRole)));
                            //(new SimpleGrantedAuthority("ROLE_USER")));
                            //(new SimpleGrantedAuthority(userService.findByUsername(username).getRole())));
        } else {
            return null;
        }
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(
                UsernamePasswordAuthenticationToken.class);
    }
}
