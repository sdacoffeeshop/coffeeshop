package com.example.coffeeshop.dto;


import java.util.List;
import java.util.Map;

public class OrderDto {

    private Long id;
    private Integer tableNo;
    private UserDto userDto;
    private String currentStatus;
    private String comment;
    private List<OrderItemDto> orderItemDtoList;
    private Long[] orderItemDtoId;
    private Integer[] orderItemQuantities;

    public Integer[] getOrderItemQuantities() {
        return orderItemQuantities;
    }

    public void setOrderItemQuantities(Integer[] orderItemQuantities) {
        this.orderItemQuantities = orderItemQuantities;
    }


    public OrderDto(Integer tableNo, UserDto userDto, String currentStatus, String comment, Long[] orderItemDtoId, Integer[] orderItemQuantities) {
        this.tableNo = tableNo;
        this.userDto = userDto;
        this.currentStatus = currentStatus;
        this.comment = comment;
        this.orderItemDtoId = orderItemDtoId;
        this.orderItemQuantities = orderItemQuantities;
    }

    public Long[] getOrderItemDtoId() {
        return orderItemDtoId;
    }

    public void setOrderItemDtoId(Long[] orderItemDtoId) {
        this.orderItemDtoId = orderItemDtoId;
    }

    public OrderDto(Long id, Integer tableNo, UserDto userDto, String currentStatus, String comment, Long[] orderItemDtoId) {
        this.id = id;
        this.tableNo = tableNo;
        this.userDto = userDto;
        this.currentStatus = currentStatus;
        this.comment = comment;
        this.orderItemDtoId = orderItemDtoId;
    }

    public OrderDto() {
    }

    public OrderDto(Long id, Integer tableNo, UserDto userDto, String currentStatus, String comment, List<OrderItemDto> list) {
        this.id = id;
        this.tableNo = tableNo;
        this.userDto = userDto;
        this.currentStatus = currentStatus;
        this.comment = comment;
        orderItemDtoList = list;
    }

    public OrderDto(Integer tableNo, UserDto userDto, String currentStatus, String comment, List<OrderItemDto> list) {
        this.tableNo = tableNo;
        this.userDto = userDto;
        this.currentStatus = currentStatus;
        this.comment = comment;
        orderItemDtoList = list;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {this.id = id;}

    public Integer getTableNo() {
        return tableNo;
    }

    public void setTableNo(Integer tableNo) {
        this.tableNo = tableNo;
    }

    public UserDto getUserDto() {
        return userDto;
    }

    public void setUserDto(UserDto userDto) {
        this.userDto = userDto;
    }

    public String getCurrentStatus() {
        return currentStatus;
    }

    public void setCurrentStatus(String currentStatus) {
        this.currentStatus = currentStatus;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }


    public void setOrderItemDtoList(List<OrderItemDto> orderItemDtoList) {
        this.orderItemDtoList = orderItemDtoList;
    }

    public List<OrderItemDto> getOrderItemDtoList() {
        return orderItemDtoList;
    }
}
