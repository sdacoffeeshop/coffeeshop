package com.example.coffeeshop.dto;

import java.math.BigDecimal;

public class OrderItemDto {
    private Long id;
    private OrderDto orderDto;
    private String productName;
    private BigDecimal productPrice;
    private Integer quantity;

    public OrderItemDto() {
    }

    public OrderItemDto(Long id, OrderDto orderDto, String productName, BigDecimal productPrice, Integer quantity) {
        this.id = id;
        this.orderDto = orderDto;
        this.productName = productName;
        this.productPrice = productPrice;
        this.quantity = quantity;
    }

    public OrderItemDto(OrderDto orderDto, String productName, BigDecimal productPrice, Integer quantity) {
        this.orderDto = orderDto;
        this.productName = productName;
        this.productPrice = productPrice;
        this.quantity = quantity;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public OrderDto getOrderDto() {
        return orderDto;
    }

    public void setOrderDto(OrderDto orderDto) {
        this.orderDto = orderDto;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public BigDecimal getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(BigDecimal productPrice) {
        this.productPrice = productPrice;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }
}
