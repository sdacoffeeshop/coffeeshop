package com.example.coffeeshop.dto;



public class UserDto {

    private Long id;

    private String username;

    private String name;

    private String password;

    private String role;

    private Boolean enabled;

    public UserDto() {
    }

    public UserDto(Long id, String username, String name, String role, Boolean enabled) {
        this.id = id;
        this.username = username;
        this.name = name;
        this.role = role;
        this.enabled = enabled;
    }

    public UserDto(String username, String password, String name, String role, Boolean enabled) {
        this.username = username;
        this.password = password;
        this.name = name;
        this.role = role;
        this.enabled = enabled;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    @Override
    public String toString() {
        return "UserDto{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}
