-- noinspection SqlNoDataSourceInspectionForFile

insert into products (name, description, price) values ('Latte', 'with milk', 10.49);
insert into products (name, description, price) values ('Macchiato', 'a lot of milch', 15.99);
insert into products (name, description, price) values ('Espresso', 'small, black coffee', 7.99);

insert into users (username, name, password, role,enabled) values('admin', 'Il Capo di Tutti Capi',
 '21232f297a57a5a743894a0e4a801fc3', 'ROLE_ADMIN',true);

 insert into users (username, name, password, role,enabled) values('user', 'Marian',
 'ee11cbb19052e40b07aac0ca060c23ee', 'ROLE_USER',true);

insert into orders(table_no, user_id, comment, current_status) values(1, 1, 'Test order no 1', 'ACCEPTED');
insert into orders(table_no, user_id, comment, current_status) values(2, 1, 'Test order no 2', 'ACCEPTED');
insert into order_items(order_id, product_name, product_price, quantity) values(1, 'Latte', 10.49, 2);
insert into order_items(order_id, product_name, product_price, quantity) values(1, 'Espresso', 7.99, 2);

insert into authorities (username,authority) values ('admin','ROLE_ADMIN');

-- nie wiem czy commit na koncu nie powinien byc