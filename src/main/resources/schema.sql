drop table if exists products;
drop table if exists order_items;
drop table if exists orders;
drop table if exists users;
drop table if exists role;
drop table if exists user_role;

create table products (id identity not null, name varchar(255), description varchar(255), price decimal(20,2), primary key(id));
create table users(id identity not null, username varchar(50) not null, name varchar(255), password varchar(255), role varchar(50), enabled boolean not null, primary key(id), unique(username));
create table orders (id identity not null, table_no integer, user_id bigint not null, comment varchar(255), current_status varchar(50), primary key(id), foreign key (user_id) references users(id));
create table order_items (id identity not null, order_id bigint not null, product_name varchar(255), product_price decimal(20,2), quantity integer, primary key(id), foreign key (order_id) references orders(id) on delete cascade);
create table authorities (username varchar (50), authority varchar (50), constraint fk_authorities_users foreign key (username) references users(username) on delete cascade on update cascade);
create unique index ix_auth_username on authorities(username,authority);