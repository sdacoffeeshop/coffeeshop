<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand">SDA Coffee Shop</a>
        </div>
        <ul class="nav navbar-nav">
            <%--<li class="active"><a href="/users">Home</a></li>--%>
                <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown">Users<span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li>
                            <spring:eval expression="@environment.getProperty('users')" var="users" />
                            <a href="${users}">Users list</a></li>
                        <li>
                            <spring:eval expression="@environment.getProperty('userAdd')" var="userAdd" />
                            <a href="${userAdd}">User Add</a></li>
                    </ul>
                </li>
                <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown">Products<span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li>
                            <spring:eval expression="@environment.getProperty('products')" var="products" />
                            <a href="${products}">Product list</a></li>
                        <li>
                            <spring:eval expression="@environment.getProperty('productAdd')" var="productAdd" />
                            <a href="${productAdd}">Add product</a></li>
                    </ul>
                </li>
            <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown">Orders<span class="caret"></span></a>
                <ul class="dropdown-menu">
                    <li>
                        <spring:eval expression="@environment.getProperty('orders')" var="orders" />
                        <a href="${orders}">Order list</a></li>
                    <li>
                        <spring:eval expression="@environment.getProperty('orderAdd')" var="orderAdd" />
                        <a href="${orderAdd}">Add order</a></li>
                </ul>
            </li>
                <li><a href="<c:url value="/logout" />">Logout</a></li>
        </ul>
    </div>
</nav>