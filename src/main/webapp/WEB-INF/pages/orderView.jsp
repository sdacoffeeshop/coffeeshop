<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <title>Order</title>
</head>
<body>

<jsp:include page="nav.jsp"></jsp:include>

<div class="container container-fluid">
    <spring:eval expression="@environment.getProperty('orderSave')" var="orderSave"/>
    <form:form action="${orderSave}" method="post" modelAttribute="orderModel">
    <c:choose>
        <c:when test="${orderModel.id eq null}">
            <h2>Add order:</h2>
            <form:hidden path="id"/>
            <form:hidden path="userDto.id"/>
            <div class="form-group">
                <form:label path="userDto.name" id="userName">User:</form:label>
                <form:input path="userDto.name" id="userName" readonly="true" cssClass="form-control"></form:input>
            </div>
        </c:when>
        <c:otherwise>
            <h2>Edit order:</h2>
            <div class="form-group">
                <form:label id="orderId" path="id">Order number:</form:label>
                <form:input readonly="true" path="id" id="orderId" cssClass="form-control"></form:input>
            </div>
            <div class="form-group">
                <form:hidden path="userDto.id"/>
                <form:label path="userDto.name" id="userName">User:</form:label>
                <form:input path="userDto.name" id="userName" readonly="true" cssClass="form-control"></form:input>
            </div>
        </c:otherwise>
    </c:choose>

    <div class="form-group">
        <form:label path="tableNo">Table number:</form:label>
        <form:input path="tableNo" id="tableNo" cssClass="form-control"></form:input>
    </div>
    <div class="form-group">
        <form:label path="comment">Comment:</form:label>
        <form:input path="comment" id="comment" cssClass="form-control"></form:input>
    </div>

    <%--<div hidden=true>--%>
        <%--<div class="form-group dropdown" id="productListTemplate">--%>
            <%--<form:label path="orderItemDtoId" id="products">Product:</form:label>--%>
            <%--<form:select path="orderItemDtoId" id="products" class="form-control" multiple="false">--%>
                <%--<form:options cssClass="dropdown-item" items="${products}"/>--%>
            <%--</form:select>--%>
        <%--</div>--%>
    <%--</div>--%>
    <%--<div class="form-group" id="productList">--%>
        <%--<div class="form-group">--%>
            <%--<a href="javascript:addProductToList();" class="btn btn-primary">Add item</a>--%>
        <%--</div>--%>
        <%--<div>--%>
            <%--<label>Order items:</label>--%>
        <%--</div>--%>
    <%--</div>--%>
<%--</div>--%>
<%--<c:forEach items="${orderModel.orderItemDtoList}" var="orderItem">--%>
    <%--<div>--%>
        <%--<p>${orderItem.productName}</p>--%>
        <%--<p>${orderItem.productPrice}</p>--%>
    <%--</div>--%>
<%--</c:forEach>--%>
<%--<script>--%>
    <%--function addProductToList() {--%>
        <%--document.getElementById("productList").innerHTML = document.getElementById("productList")--%>
            <%--.innerHTML + document.getElementById("productListTemplate").innerHTML;--%>
    <%--}--%>
<%--</script>--%>




    <div class="form-group dropdown">
        <form:label path="currentStatus" id="currentStatus">Status:</form:label>
        <form:errors path="currentStatus" id="currentStatus" cssClass="alert alert-danger" element="div"
                     cssStyle="max-width: 50%"></form:errors>
        <form:select path="currentStatus" id="currentStatus" class="form-control">
            <form:options cssClass="dropdown-item" items="${statuses}" itemValue="status" itemLabel="status"/>
        </form:select>
    </div>

    <div id="productList">

    </div>

    <a href="javascript:addProductToList();">Dodaj produkt</a>

    <div>
        <table class="table table-striped">
            <thead>
            <tr>
                <th>Product</th>
                <th>Price</th>
                <th>Quantity</th>
            </tr>
            </thead>
            <tbody>
            <c:forEach items="${orderModel.orderItemDtoList}" var="orderItem">
                <tr>
                    <td>${orderItem.productName}</td>
                    <td>${orderItem.productPrice}</td>
                    <td>${orderItem.quantity}</td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </div>



    <script>
        function addProductToList() {
            document.getElementById("productList").innerHTML = document.getElementById("productList")
                    .innerHTML + '        <div class="form-group dropdown" class="product">'+
                '        <label for="orderItemDtoList" id="productsModel">Select product:</label>'+
                '        <select id="productsModel" name="orderItemDtoId" class="form-control">'+
                '          <option class="dropdown-item" value="1">Latte</option>'+
                '          <option class="dropdown-item" value="2">Macchiato</option>'+
                '          <option class="dropdown-item" value="3">Espresso</option>'+
                '        </select>'+
                '    </div>'+
                '   <div class = "form-group" class="quantity">' +
                '<label for="orderItemQuantities" id="productsModel" name="orderItemQuantities"> Quantity </label>' +
                '<input type="number" class="form-control" id="orderItemQuantities" name="orderItemQuantities"> ' +
                '</div>';
        }
    </script>






</div>
<div class="container container-fluid">
</form:form>
</div>
</body>
</html>
