<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <title>Product</title>
</head>
<body>
<jsp:include page="nav.jsp"></jsp:include>

<div class="container">

<div class="container container-fluid">
    <spring:eval expression="@environment.getProperty('productSave')" var="productSave" />
    <form:form action="${productSave}" method="post" modelAttribute="productModel">
        <c:choose>
            <c:when test="${productModel.id eq null}">
                <h2>Add product:</h2>
                <form:hidden path="id"/>
            </c:when>
            <c:otherwise>
                <h2>Edit product:</h2>
                <div class="form-group">
                    <form:label id="productId" path="id">Product number:</form:label>
                    <form:input readonly="true" path="id" id="productId" cssClass="form-control"></form:input>
                </div>
            </c:otherwise>
        </c:choose>

        <div class="form-group">
            <form:label path="name" id="productName">Product name</form:label>
            <form:errors path="name" id="productName" cssClass="alert alert-danger" element="div"
                         cssStyle="max-width: 50%"></form:errors>
            <form:input path="name" cssClass="form-control" id="productName" placeholer="enter name"></form:input>
        </div>
        <div class="form-group">
            <form:label path="description" id="description">Product description: </form:label>
            <form:errors path="description" id="description" cssClass="alert alert-danger" element="div"
                         cssStyle="max-width: 50%"></form:errors>
            <form:input path="description" cssClass="form-control" id="description"
                        placeholer="enter description"></form:input>
        </div>
        <div class="form-group">
            <form:label path="price" id="productPrice">Product price</form:label>
            <form:errors path="price" id="productPrice" cssClass="alert alert-danger" element="div"
                         cssStyle="max-width: 50%"></form:errors>
            <form:input path="price" cssClass="form-control" id="productPrice" placeholder="enter price"></form:input>
        </div>
        <button type="submit" class="btn btn-primary">Save</button>
    </form:form>
</div>
</div>

</body>
</html>