<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <title>Order Lists</title>

</head>
<body>
<jsp:include page="nav.jsp"></jsp:include>

<div class="container">
    <h2>Order list</h2>
    <table class="table table-striped">
        <thead>
        <tr>
            <th>Id</th>
            <th>table number</th>
            <th>User id</th>
            <th>CurrentStauts</th>
            <th>Comment</th>
<%-- potem odkomentować           <th>Total value</th> --%>
            <th></th>
        </tr>
        </thead>
        <tbody>
        <c:forEach items="${orderList}" var="order">
            <tr>
                <td>${order.id}</td>
                <td>${order.tableNo}</td>
                <td>${order.userDto.name}</td>
                <td>${order.currentStatus}</td>
                <td>${order.comment}</td>
<%-- odkomentować                <td><${order.totalValue}</td> --%>
                <td>
                    <spring:eval expression="@environment.getProperty('orderId')" var="orderId" />
                    <a href="${orderId}${order.id}">Edit</a></td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
</div>

</body>
</html>