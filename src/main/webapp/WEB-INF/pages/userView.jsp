<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <title>User</title>

</head>
<body>
<jsp:include page="nav.jsp"></jsp:include>

<div class="container container-fluid">
    <spring:eval expression="@environment.getProperty('userSave')" var="userSave"/>
    <form:form action="${userSave}" method="post" modelAttribute="userModel">
        <c:choose>
            <c:when test="${userModel.id eq null}">
                <h2>Register user</h2>
            </c:when>
            <c:otherwise>
                <h2>Modify user</h2>
            </c:otherwise>

        </c:choose>
        <form:hidden path="id"/>
        <div class="form-group">

            <c:choose>
                <c:when test="${userModel.id eq null}">

                    <form:label path="username" id="username">Login</form:label>
                    <form:input path="username" cssClass="form-control" id="username"></form:input>
                </c:when>
                <c:otherwise>

                    <label>Login</label>
                    <p class="form-control">${userModel.username}</p>
                </c:otherwise>
            </c:choose>

        </div>
        <div class="form-group">
            <form:label path="name" id="name">User name</form:label>
            <form:input path="name" cssClass="form-control" id="name"></form:input>
        </div>

        <c:choose>
            <c:when test="${userModel.id eq null}">
                <div class="form-group">
                    <form:label path="password" id="password">Password </form:label>
                    <form:password path="password" cssClass="form-control" id="password"></form:password>
                </div>
            </c:when>
            <c:otherwise>
                <form:hidden path="password"/>
            </c:otherwise>
        </c:choose>

        <div class="form-group dropdown">
            <form:label path="role" id="role">Role</form:label>
            <form:select path="role" id="role" class="form-control">
                <form:options cssClass="dropdown-item" items="${roles}"/>
            </form:select>
        </div>

        <div class="form-group">
            <form:label path="enabled" id="enabled" cssClass="inline">Active</form:label>
            <form:checkbox path="enabled" id="enabled" class="form-control" cssClass="inline"/>
        </div>
        <div class="form-group">
            <button type="Save" class="btn btn-primary">Save</button>
        </div>
    </form:form>
</div>

</body>
</html>