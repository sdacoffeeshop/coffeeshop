<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <title>User List</title>

</head>
<body>
<jsp:include page="nav.jsp"></jsp:include>
<div class="container">
    <h2>Users</h2>
    <table class="table table-striped">
        <thead>
        <tr>
            <th>Id</th>
            <th>Login</th>
            <th>Name</th>
            <th>Role</th>
            <th>Active</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        <c:forEach items="${userList}" var="user">
            <tr>
                <td>${user.id}</td>
                <td>${user.username}</td>
                <td>${user.name}</td>
                <td>${user.role}</td>
                <td><input type="checkbox" name="userEnabled" checked="${user.enabled}"  disabled="true"/></td>
                <td>
                    <spring:eval expression="@environment.getProperty('userId')" var="userId" />
                    <a href="${userId}${user.id}">Edit</a> </td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
</div>


</body>
</html>