package com.example.coffeeshop;

import org.junit.Test;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.apache.commons.codec.digest.DigestUtils;

public class BcryptTest {

    @Test
    public void bcryptTest(){

        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        String encodedPassword = encoder.encode("admin");
        System.out.println(encodedPassword);
//$2a$10$zDHv9bIsZEWqkuzTrizZJOa6TBULNMHXo0wOqUkQLICPSt08jWYAG
    }

    @Test
    public void md5HexAdminTest(){
        String password = DigestUtils.md5Hex("admin");
        System.out.println(password);
    }

    @Test
    public void md5HexUserTest(){
        String password = DigestUtils.md5Hex("user");
        System.out.println(password);
    }
}
