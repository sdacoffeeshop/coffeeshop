package com.example.coffeeshop.asserts;

import com.example.coffeeshop.entity.Product;
import org.junit.Assert;

import java.util.List;

public class ProductListAssert implements ListAssert {
    private List<Product> productList;

    public ProductListAssert(List<Product> productList) {
        this.productList = productList;
    }

    public ListAssert hasItems(int listSize) {
        Assert.assertEquals(listSize, productList.size());
        return this;
    }

    public ListAssert hasItemById(Long itemId) {
        Assert.assertTrue(hasId(itemId));
        return this;
    }

    public ListAssert isDeletedById(Long itemId) {
        Assert.assertFalse(hasId(itemId));
        return this;
    }

    public Boolean hasId(Long id) {
        for(Product product : productList) {
            if (product.getId() == id) {
                return true;
            }
        }
        return false;
    }
}
