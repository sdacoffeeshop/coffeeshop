package com.example.coffeeshop.asserts;

import com.example.coffeeshop.dto.OrderDto;
import org.junit.Assert;

import java.util.List;

public class OrderDtoListAssert implements ListAssert {
    private List<OrderDto> orderDtoList;

    public OrderDtoListAssert(List<OrderDto> orderDtoList) {
        this.orderDtoList = orderDtoList;
    }

    public ListAssert hasItems(int listSize) {
        Assert.assertEquals(listSize, orderDtoList.size());
        return this;
    }

    public ListAssert hasItemById(Long itemId) {
        Assert.assertTrue(hasId(itemId));
        return this;
    }

    public ListAssert isDeletedById(Long itemId) {
        Assert.assertFalse(hasId(itemId));
        return this;
    }

    public Boolean hasId(Long id) {
        for(OrderDto order: orderDtoList) {
            if (order.getId() == id) {
                return true;
            }
        }
        return false;
    }
}
