package com.example.coffeeshop.asserts;

import com.example.coffeeshop.entity.OrderItem;
import org.junit.Assert;

import java.util.List;

public interface ListAssert {

    public ListAssert hasItems(int listSize);

    public ListAssert hasItemById(Long itemId);

    public ListAssert isDeletedById(Long itemId);

    public Boolean hasId(Long id);
}
