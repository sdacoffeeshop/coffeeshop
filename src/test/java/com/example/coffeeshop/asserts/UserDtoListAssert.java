package com.example.coffeeshop.asserts;

import com.example.coffeeshop.dto.UserDto;
import org.junit.Assert;

import java.util.List;

public class UserDtoListAssert implements ListAssert{
    private List<UserDto> userList;

    public UserDtoListAssert(List<UserDto> userList) {
        this.userList = userList;
    }

    public ListAssert hasItems(int listSize) {
        Assert.assertEquals(listSize, userList.size());
        return this;
    }

    public ListAssert hasItemById(Long itemId) {
        Assert.assertTrue(hasId(itemId));
        return this;
    }

    public ListAssert isDeletedById(Long itemId) {
        Assert.assertFalse(hasId(itemId));
        return this;
    }

    public Boolean hasId(Long id) {
        for(UserDto user : userList) {
            if (user.getId() == id) {
                return true;
            }
        }
        return false;
    }
}
