package com.example.coffeeshop.asserts;

import com.example.coffeeshop.entity.Order;
import org.junit.Assert;

import java.util.List;

public class OrderListAssert implements ListAssert{
    private List<Order> orderList;

    public OrderListAssert(List<Order> orderList) {
        this.orderList = orderList;
    }

    public ListAssert hasItems(int listSize) {
        Assert.assertEquals(listSize, orderList.size());
        return this;
    }

    public ListAssert hasItemById(Long itemId) {
        Assert.assertTrue(hasId(itemId));
        return this;
    }

    public ListAssert isDeletedById(Long itemId) {
        Assert.assertFalse(hasId(itemId));
        return this;
    }

    public Boolean hasId(Long id) {
        for(Order order : orderList) {
            if (order.getId() == id) {
                return true;
            }
        }
        return false;
    }



}
