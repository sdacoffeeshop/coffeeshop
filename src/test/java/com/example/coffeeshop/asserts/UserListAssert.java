package com.example.coffeeshop.asserts;

import com.example.coffeeshop.entity.User;
import org.junit.Assert;

import java.util.List;

public class UserListAssert implements ListAssert{
    private List<User> userList;

    public UserListAssert(List<User> userList) {
        this.userList = userList;
    }

    public ListAssert hasItems(int listSize) {
        Assert.assertEquals(listSize, userList.size());
        return this;
    }

    public ListAssert hasItemById(Long itemId) {
        Assert.assertTrue(hasId(itemId));
        return this;
    }

    public ListAssert isDeletedById(Long itemId) {
        Assert.assertFalse(hasId(itemId));
        return this;
    }

    public Boolean hasId(Long id) {
        for(User user : userList) {
            if (user.getId() == id) {
                return true;
            }
        }
        return false;
    }
}
