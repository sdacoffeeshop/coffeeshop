package com.example.coffeeshop.asserts;

import com.example.coffeeshop.dto.ProductDto;
import org.junit.Assert;

import java.util.List;

public class ProductDtoListAssert implements ListAssert{
    private List<ProductDto> productList;

    public ProductDtoListAssert(List<ProductDto> productList) {
        this.productList = productList;
    }

    public ListAssert hasItems(int listSize) {
        Assert.assertEquals(listSize, productList.size());
        return this;
    }

    public ListAssert hasItemById(Long itemId) {
        Assert.assertTrue(hasId(itemId));
        return this;
    }

    public ListAssert isDeletedById(Long itemId) {
        Assert.assertFalse(hasId(itemId));
        return this;
    }

    public Boolean hasId(Long id) {
        for(ProductDto product : productList) {
            if (product.getId() == id) {
                return true;
            }
        }
        return false;
    }
}
