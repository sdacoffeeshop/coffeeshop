package com.example.coffeeshop.asserts;

import com.example.coffeeshop.entity.OrderItem;
import org.junit.Assert;

import java.util.List;

public class OrderItemListAssert implements ListAssert {
    private List<OrderItem> orderItemList;

    public OrderItemListAssert(List<OrderItem> orderItemList) {
        this.orderItemList = orderItemList;
    }

    public ListAssert hasItems(int listSize) {
        Assert.assertEquals(listSize, orderItemList.size());
        return this;
    }

    public ListAssert hasItemById(Long itemId) {
        Assert.assertTrue(hasId(itemId));
        return this;
    }

    public ListAssert isDeletedById(Long itemId) {
        Assert.assertFalse(hasId(itemId));
        return this;
    }

    public Boolean hasId(Long id) {
        for(OrderItem orderItem : orderItemList) {
            if (orderItem.getId() == id) {
                return true;
            }
        }
        return false;
    }
}
