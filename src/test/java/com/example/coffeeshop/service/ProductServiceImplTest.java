package com.example.coffeeshop.service;

import com.example.coffeeshop.asserts.ProductDtoListAssert;
import com.example.coffeeshop.dto.ProductDto;
import com.example.coffeeshop.exceptions.OrderException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ProductServiceImplTest {

    @Autowired
    ProductService productService;

    @Test
    public void shouldFindAll() throws OrderException {
        List<ProductDto> products =  productService.findAll();
        new ProductDtoListAssert(products).hasItems(3);
    }

    @Test
    public void shouldFindById() throws OrderException {
        List<ProductDto> products = productService.findAll();
        ProductDto product = productService.findById(products.get(0).getId());
        new ProductDtoListAssert(products).hasItemById(products.get(0).getId());
    }

    @Test
    public void shouldSave() throws OrderException {
        List<ProductDto> products = productService.findAll();
        Integer beforeSize = products.size();
        productService.save(new ProductDto("Black", "without milk", BigDecimal.valueOf(25.99)));
        products = productService.findAll();
        new ProductDtoListAssert(products).hasItems(beforeSize + 1);
    }
}