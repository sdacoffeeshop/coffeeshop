package com.example.coffeeshop.service;

import com.example.coffeeshop.asserts.ProductDtoListAssert;
import com.example.coffeeshop.asserts.UserDtoListAssert;
import com.example.coffeeshop.asserts.UserListAssert;
import com.example.coffeeshop.dto.ProductDto;
import com.example.coffeeshop.dto.UserDto;
import com.example.coffeeshop.entity.User;
import com.example.coffeeshop.exceptions.OrderException;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserServiceImplTest {

    @Autowired
    UserService userService;



    @Test
    public void shouldFindAll() throws OrderException {
        List<UserDto> users = userService.findAll();
        new UserDtoListAssert(users).hasItems(2);
    }

    @Test
    public void shouldFindById() throws OrderException {
        List<UserDto> users = userService.findAll();
        UserDto user = userService.findById(users.get(0).getId());
        new UserDtoListAssert(users).hasItemById(user.getId());
    }

    @Test
    public void shouldFindByUsername() {
        List<UserDto> users = userService.findAll();
        UserDto user = userService.findByUsername(users.get(0).getUsername());
        new UserDtoListAssert(users).hasItemById(user.getId());
    }

    @Test
    public void save() {
        List<UserDto> users = userService.findAll();
        Integer beforeSize = users.size();
        userService.save(new UserDto("testAdmin1", "12345", "test", "ROLE_ADMIN", true));
        users = userService.findAll();
        new UserDtoListAssert(users).hasItems(beforeSize + 1);
    }

    @Test
    public void shouldAuthenticate() {
        userService.save(new UserDto("testAdmin2", "12345", "test", "ROLE_ADMIN", true));
        Boolean isAuthenticate = userService.authenticate("testAdmin2", "12345");
        Assert.assertTrue(isAuthenticate);
    }
}