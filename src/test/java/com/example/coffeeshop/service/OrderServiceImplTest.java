package com.example.coffeeshop.service;

import com.example.coffeeshop.asserts.OrderDtoListAssert;
import com.example.coffeeshop.auxiliary.Status;
import com.example.coffeeshop.dto.OrderItemDto;
import com.example.coffeeshop.dto.UserDto;
import com.example.coffeeshop.entity.Order;
import com.example.coffeeshop.exceptions.OrderException;
import com.example.coffeeshop.dto.OrderDto;
import com.example.coffeeshop.exceptions.OrderItemException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;


@RunWith(SpringRunner.class)
@SpringBootTest
public class OrderServiceImplTest {
    @Autowired
    OrderService orderService;

    @Autowired
    OrderItemService orderItemService;

    @Autowired
    UserService userService;

    @Test
    public void shouldFindAll() throws OrderException {
        List<OrderDto> orders = orderService.findAll();
        new OrderDtoListAssert(orders).hasItems(3);
    }

    @Test
    public void shouldFindById() throws OrderException {
        List<OrderDto> orders = orderService.findAll();
        OrderDto order = orderService.findById(orders.get(0).getId());
        new OrderDtoListAssert(orders).hasItemById(order.getId());
    }

    @Test
    public void shouldSaveItem() throws OrderException {
        List<OrderDto> orders = orderService.findAll();
        int beforeSize = orders.size();
        orderService.saveOrUpdate(new OrderDto(4, userService.findByUsername("admin"), "ACCEPTED", "ENTERED", null));
        orders = orderService.findAll();
        new OrderDtoListAssert(orders).hasItems(beforeSize + 1);
    }

    @Test
    public void shouldSaveNewItem() throws OrderException {
        UserDto userDto = userService.findById(1l);
        List<OrderItemDto> list = new ArrayList<>();
        OrderDto orderDto = new OrderDto(1, userDto, "ACCEPTED", "New test order", list);
        list.add(new OrderItemDto( orderDto, "Herbata", BigDecimal.valueOf(16.99), 1));
        orderDto = orderService.saveOrUpdate(new OrderDto(4, userService.findByUsername("admin"), "ACCEPTED", "ENTERED", null));

        OrderDto finalOrderDto = orderDto;
        list.stream().forEach(item -> {
            try {
                item.setOrderDto(finalOrderDto);
                orderItemService.saveOrUpdate(item);
            } catch (OrderItemException e) {
                e.printStackTrace();
            }
        });
        List<OrderDto> orders = orderService.findAll();
       // new OrderDtoListAssert(orders).hasItems(beforeSize + 1);
    }
}