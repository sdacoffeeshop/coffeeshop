package com.example.coffeeshop.service;

import com.example.coffeeshop.asserts.OrderItemDtoListAssert;
import com.example.coffeeshop.dto.OrderItemDto;
import com.example.coffeeshop.exceptions.OrderException;
import com.example.coffeeshop.exceptions.OrderItemException;
import com.example.coffeeshop.repository.OrderRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.util.List;


@RunWith(SpringRunner.class)
@SpringBootTest
public class OrderItemServiceImplTest {

    @Autowired
    OrderItemService orderItemService;

    @Autowired
    OrderService orderService;

    @Autowired
    OrderRepository orderRepository;

    @Test
    public void shouldFindAll() throws OrderItemException {
        List<OrderItemDto> ordersItem = orderItemService.findAll();
        new OrderItemDtoListAssert(ordersItem).hasItems(2);
    }
    @Test
    public void shouldFindById() throws OrderItemException {
        List<OrderItemDto> ordersItem = orderItemService.findAll();
        OrderItemDto orderItem = orderItemService.findById(ordersItem.get(0).getId());
        new OrderItemDtoListAssert(ordersItem).hasItemById(orderItem.getId());
    }

    @Test
    public void shouldSaveItem() throws OrderItemException, OrderException {
        List<OrderItemDto> ordersItem = orderItemService.findAll();
        int beforeSize = ordersItem.size();
        orderItemService.saveOrUpdate(new OrderItemDto(orderService.findById(1l), "Macchiato", BigDecimal.valueOf(15.99), 5));
        ordersItem = orderItemService.findAll();
        new OrderItemDtoListAssert(ordersItem).hasItems(beforeSize+1);
    }
}