package com.example.coffeeshop.repository;

import com.example.coffeeshop.asserts.OrderListAssert;
import com.example.coffeeshop.entity.Order;
import com.example.coffeeshop.exceptions.OrderException;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.Optional;

@RunWith(SpringRunner.class)
@DataJpaTest
public class OrderRepositoryTest {

    @Autowired
    OrderRepository orderRepository;

    @Test
    public void shouldFindAll() throws OrderException {
        //Test data from test/resources/data.sql
        List<Order> orders = (List<Order>) orderRepository.findAll();
        new OrderListAssert(orders).hasItems(2);
    }

    @Test
    public void shouldFindById() throws OrderException {
        //Test data from test/resources/data.sql
        List<Order> orders = (List<Order>) orderRepository.findAll();
        Optional<Order> order = orderRepository.findById(orders.get(0).getId());
        new OrderListAssert(orders).hasItemById(order.get().getId());
    }

    @Test
    public void shouldDeleteById() throws OrderException {
        //Test data from test/resources/data.sql
        List<Order> orders = (List<Order>) orderRepository.findAll();
        Long id = orders.get(0).getId();
        orderRepository.deleteById(id);
        orders = (List<Order>) orderRepository.findAll();
        new OrderListAssert(orders).isDeletedById(id);
    }
}