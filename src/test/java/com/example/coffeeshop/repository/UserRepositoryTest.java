package com.example.coffeeshop.repository;

import com.example.coffeeshop.asserts.UserListAssert;
import com.example.coffeeshop.entity.Order;
import com.example.coffeeshop.entity.OrderItem;
import com.example.coffeeshop.entity.Role;
import com.example.coffeeshop.entity.User;
import com.example.coffeeshop.exceptions.OrderException;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.Optional;

@RunWith(SpringRunner.class)
@DataJpaTest
public class UserRepositoryTest {
    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    UserRepository userRepository;


    @Test
    public void shouldFindAll() throws OrderException {
        List<User> users = userRepository.findAll();
        new UserListAssert(users).hasItems(2);
    }


    @Test
    public void shouldFindById() throws OrderException {
        List<User> users = userRepository.findAll();
        Optional<User> user = userRepository.findById(users.get(0).getId());
        new UserListAssert(users).hasItemById(user.get().getId());
    }

    @Test
    public void shouldDeleteById() throws OrderException {
        List<User> users = userRepository.findAll();
        Long id = users.get(1).getId();
        userRepository.deleteById(id);
        users = userRepository.findAll();
        new UserListAssert(users).isDeletedById(id);
    }

    @Test
    public void shouldFindOneByUsernameAndPasswordAndEnabled() throws OrderException {
        List<User> users = userRepository.findAll();
        User user = userRepository.findOneByUsernameAndPasswordAndEnabled(users.get(0).getUsername(), users.get(0).getPassword(), true);
        Assert.assertTrue(user != null);
    }
}