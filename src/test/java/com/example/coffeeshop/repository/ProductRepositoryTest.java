package com.example.coffeeshop.repository;

import com.example.coffeeshop.asserts.OrderListAssert;
import com.example.coffeeshop.asserts.ProductListAssert;
import com.example.coffeeshop.entity.Order;
import com.example.coffeeshop.entity.Product;
import com.example.coffeeshop.exceptions.OrderException;
import com.example.coffeeshop.repository.ProductRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

@RunWith(SpringRunner.class)
@DataJpaTest
public class ProductRepositoryTest {
    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    ProductRepository productRepository;

    @Test
    public void shouldFindAll() throws OrderException {
        List<Product> products = (List<Product>) productRepository.findAll();
        new ProductListAssert(products).hasItems(3);
    }

    @Test
    public void shouldFindById() throws OrderException {
        List<Product> products = (List<Product>) productRepository.findAll();
        Optional<Product> product = productRepository.findById(products.get(0).getId());
        new ProductListAssert(products).hasItemById(product.get().getId());
    }

    @Test
    public void shouldDeleteById() throws OrderException {
        List<Product> products = (List<Product>) productRepository.findAll();
        Long id = products.get(0).getId();
        productRepository.deleteById(id);
        products = (List<Product>) productRepository.findAll();
        new ProductListAssert(products).isDeletedById(id);
    }
}