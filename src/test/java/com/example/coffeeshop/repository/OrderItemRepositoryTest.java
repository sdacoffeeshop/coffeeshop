package com.example.coffeeshop.repository;

import com.example.coffeeshop.asserts.OrderItemListAssert;
import com.example.coffeeshop.entity.OrderItem;
import com.example.coffeeshop.exceptions.OrderException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.Optional;

@RunWith(SpringRunner.class)
@DataJpaTest
public class OrderItemRepositoryTest {

    @Autowired
    OrderItemRepository orderItemRepository;


    @Test
    public void shouldFindAll() throws OrderException {
        //Test data from main/resources/data.sql
        List<OrderItem> orderItems = (List<OrderItem>) orderItemRepository.findAll();
        new OrderItemListAssert(orderItems).hasItems(2);
    }

   @Test
    public void shouldFindById() throws OrderException {
       List<OrderItem> orderItems = (List<OrderItem>) orderItemRepository.findAll();
       Optional<OrderItem> orderItem = orderItemRepository.findById(orderItems.get(0).getId());
       new OrderItemListAssert(orderItems).hasItemById(orderItem.get().getId());
    }

    @Test
    public void shouldDeleteById() throws OrderException {
        List<OrderItem> ordersItem = (List<OrderItem>) orderItemRepository.findAll();
        Long id = ordersItem.get(0).getId();
        orderItemRepository.deleteById(id);
        ordersItem = (List<OrderItem>) orderItemRepository.findAll();
        new OrderItemListAssert(ordersItem).isDeletedById(id);
    }
}
