-- noinspection SqlNoDataSourceInspectionForFile

insert into products (name, description, price) values ('Latte', 'with milk', 10.49);
insert into products (name, description, price) values ('Macchiato', 'a lot of milch', 15.99);
insert into products (name, description, price) values ('Espresso', 'small, black coffee', 7.99);

insert into users (username, name, password, role) values('admin', 'Il Capo di Tutti Capi', '', 'ROLE_ADMIN');

insert into orders(table_no, user_id, comment, current_status) values(1, 1, 'Test order no 1', 'ENTERED');
insert into orders(table_no, user_id, comment, current_status) values(2, 1, 'Test order no 2', 'ENTERED');
insert into orders(table_no, user_id, comment, current_status) values(3, 1, 'Test order no 3', 'ENTERED');

insert into order_items(order_id, product_name, product_price, quantity) values(1, 'Latte', 10.49, 2);
insert into order_items(order_id, product_name, product_price, quantity) values(1, 'Espresso', 7.99, 3);
insert into order_items(order_id, product_name, product_price, quantity) values(1, 'Macchiato', 15.99, 1);